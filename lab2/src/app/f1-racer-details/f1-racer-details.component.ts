import {Component, Input, OnInit} from '@angular/core';
import {F1Racer} from "../model/F1-Racer";
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import {F1RacerManagementService} from "../f1-racer-management.service";
import 'rxjs/add/operator/switchMap';
import {Router} from "@angular/router";

import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-f1-racer-details',
  templateUrl: './f1-racer-details.component.html',
})
export class F1RacerDetailsComponent implements OnInit {

  constructor(
    private f1racerService: F1RacerManagementService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  @Input('f1racerDetails')
  f1Racer: F1Racer;

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        const f1racerNumber = params.get('number');
        const f1racerPromise = this.f1racerService.getF1Racer(f1racerNumber);
        f1racerPromise.catch(
          error => {
            console.error(error.errorMessage);
          }
        );
        return f1racerPromise;
      })
      .subscribe(f1racer => {
        this.f1Racer = f1racer;

      });
  }


  goEdit(): void {
    this.router.navigate(['/editor', this.f1Racer.number]);
  }
  goBack(): void {
    this.router.navigate(['/f1racers']);
  }


}
