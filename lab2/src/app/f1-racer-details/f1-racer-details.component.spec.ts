import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F1RacerDetailsComponent } from './f1-racer-details.component';

describe('F1RacerDetailsComponent', () => {
  let component: F1RacerDetailsComponent;
  let fixture: ComponentFixture<F1RacerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F1RacerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F1RacerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
