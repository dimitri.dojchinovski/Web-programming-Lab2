import { Component,OnInit } from '@angular/core';
import {F1Racer} from "./model/F1-Racer";
import {F1RacerManagementService} from "./f1-racer-management.service";

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit{
  f1racers: F1Racer[];
  constructor(private f1RacersService:F1RacerManagementService){}

  ngOnInit():void{
    this.f1RacersService.observedF1Racers.subscribe(f1racers => this.f1racers = f1racers.slice(0,3));
  }

}
