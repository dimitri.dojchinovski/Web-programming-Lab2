import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';
import {Router} from "@angular/router";
import {F1RacerManagementService} from "./f1-racer-management.service";
import {F1Racer} from "./model/F1-Racer";
import {F1} from "codelyzer/util/function";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
@Component({
  selector: 'new-racer',
  templateUrl: './new.component.html',
})
export class NewComponent{

  rForm: FormGroup;
  post: any;
  name:string = '';
  team:string = '';
  number:string = '';


  public CREATE_ACTION = 'Create';

  private _editingVideo: F1Racer;

  private action = this.CREATE_ACTION;


  protected newRacer: F1Racer;

  constructor(private router: Router,
              private f1RacersService: F1RacerManagementService,
              private locationService: Location,
              private fb: FormBuilder) {
    this.newRacer = new F1Racer();
    this.rForm=fb.group({
      'name':[null, Validators.required],
      'number':[null, Validators.compose([Validators.required, Validators.maxLength(2)])],
      'team':[null,Validators.required]
    });
  }
  private setVideo(editingVideo: F1Racer) {
    console.log('Setting video');
    // if the editingVideo is not set, we should prevent the error
    if (editingVideo) {
      this._editingVideo = editingVideo;
      this.newRacer.name = editingVideo.name;
      this.newRacer.team = editingVideo.team;
      this.newRacer.number = editingVideo.number;
      this.newRacer.id = editingVideo.id;
    }
  }

  public save(): void {
    console.log('saving...');

    this.f1RacersService.save(this.newRacer)
      .then(videoFromServer => this.setVideo(videoFromServer));
    // we should reset the video instance after saving it
    localStorage.setItem(this.newRacer.number, JSON.stringify(this.newRacer));
    this.newRacer = new F1Racer();
  }




}
