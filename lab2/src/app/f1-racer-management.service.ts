import { Injectable } from '@angular/core';
import {F1Racer} from "./model/F1-Racer";

import {BehaviorSubject} from "rxjs/BehaviorSubject";




@Injectable()
export class F1RacerManagementService {

  private idSequence = 4;

  private videoSource = new BehaviorSubject<F1Racer[]>([
    { name: "Lewis Hamilton", team: 'Petronas AMG', number:"44" , id:1 },
    { name: "Sebastian Vettel", team: 'Ferrari', number:"5" , id:2 },
    { name: "Kimi Raikonen", team: 'Ferrari', number:"7" , id:3 },
    { name: "Veltari Bottas", team: 'Petronas AMG', number:"77" , id:4 }]);

  observedF1Racers = this.videoSource.asObservable();


  observedVideos = this.videoSource.asObservable();

  getF1Racer(number: string): Promise<F1Racer> {
    console.log(number);
    console.log(this.videoSource.getValue());
    const result = this.videoSource.getValue().filter(f1racer => f1racer.number === number);
    if (result && result.length > 0) {
      return Promise.resolve(result[0]);
    } else {
      return Promise.reject({
        errorMessage: 'No video with the given title found',
        errorCode: 404
      });
    }
  }



    save(f1racer: F1Racer): Promise<F1Racer> {
    // simulation of the change that the async call will make
    const f1racersFromServer = [];
    Object.assign(f1racersFromServer, this.videoSource.getValue());

    this.idSequence++;
    f1racer.id = this.idSequence;
    f1racersFromServer.push(f1racer);
    console.log("ALO"+f1racer);

    this.videoSource.next(f1racersFromServer);

    return Promise.resolve(f1racer);
  }

  edit(originalF1Racer: F1Racer, updatedF1Racer: F1Racer): Promise<F1Racer> {
    // simulation of the change that the async call will make
    const f1racersFromServer = [];
    Object.assign(f1racersFromServer, this.videoSource.getValue());

    const f1racerToChange = f1racersFromServer.find(i => i.id === originalF1Racer.id);
    Object.assign(f1racerToChange, updatedF1Racer);
    this.videoSource.next(f1racersFromServer);

    return Promise.resolve(updatedF1Racer);
  }







}
